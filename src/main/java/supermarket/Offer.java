package supermarket;


import java.time.LocalDate;

abstract class Offer implements Promotion {

    abstract public boolean applies(int scannedCount);

    private final LocalDate expiry;

    Offer(LocalDate expiry) {
        this.expiry = expiry;
    }

    public double price() {
        return 0; // Override where necessary to implement other offer prices
    }

    boolean notExpired() {
        return expiry.isAfter(LocalDate.now());
    }
}
