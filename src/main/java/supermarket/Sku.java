package supermarket;


public class Sku extends SkuAdaptor {

    private final Promotion promotion;
    private final double price;

    Sku(Promotion promotion, double price) {
        this.promotion = promotion;
        this.price = price;
    }

    @Override
    public double getPrice(int scannedCount) {
        if(promotion.applies(scannedCount)) {
            return promotion.price();
        }

        return price;
    }
}
