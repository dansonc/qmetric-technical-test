package supermarket;


import java.time.LocalDate;

public class BuyOneGetOneFree extends Offer {

    BuyOneGetOneFree(LocalDate expiry) {
        super(expiry);
    }

    @Override
    public boolean applies(int scannedCount) {
        return scannedCount % 2 == 0 && notExpired();
    }
}
