package supermarket;

import java.util.HashMap;
import java.util.Map;

class Session {

    private double total;
    private final Map<Item, Integer> scannedItems = new HashMap<>();

    void add(Item item, PriceRetriever priceRetriever) {
        storeItem(item);
        total += priceRetriever.getPrice(scannedCount(item));
    }

    void addWeighedItem(Item item, PriceRetriever priceRetriever, double weightInKg) {
        storeItem(item);
        total += priceRetriever.getPrice(weightInKg);
    }

    private void storeItem(Item item) {
        scannedItems.put(item, scannedCount(item)+1);
    }

    private int scannedCount(Item item) {
        if(scannedItems.containsKey(item))
            return scannedItems.get(item);

        return 0;
    }

    double getTotal() {
        return total;
    }
}
