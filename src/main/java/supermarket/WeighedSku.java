package supermarket;


public class WeighedSku extends SkuAdaptor{

    private final double pricePerKg;

    WeighedSku(double pricePerKg){
        this.pricePerKg = pricePerKg;
    }

    @Override
    public double getPrice(double weightInKg){

        return pricePerKg * weightInKg;
    }
}
