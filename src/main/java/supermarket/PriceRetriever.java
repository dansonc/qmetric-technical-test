package supermarket;

public interface PriceRetriever {

    double getPrice(int scannedCount);

    double getPrice(double weightInKg);
}
