

I am relatively happy with the roles & responsibilities I have assigned to the classes in the design of this code
in the time I have given to it.

I think the decision with the greatest impact on the design was the addition to PriceRetriever of the method
to fetch price based on a weighed quantity. This led me down the path of creating a small class hierarchy with
SkuAdaptor, Sku & WeighedSku. I am not entirely happy with the fact that the base class contains an implementation of
PriceRetriever that is only needed in part by each of the sub-classes. If this was production code I would not wish
to commit this solution.

It would be more appropriate to have a separate interface WeighedPriceRetriever implemented by WeighedSku and then
the class hierarchy goes away. The Session method to add a weighed item would then look like this -->

void addWeighedItem(Item item, WeighedPriceRetriever weighedPriceRetriever, double weightInKg)

The implementation then just asks for the price from the weighedPriceRetriever

One thing I do like in the current solution is that in the real world, for weighed goods, the offer is always
expressed in the price per Kg. The Session class adds ONION as a single Item and the PriceRetriever takes care of
the price. This makes it very easy to express an offer for weighed goods.

There will be cause at some point for implementing hashCode() and equals(). If 2 lots of ONION are added to the Session
and we need to Void one of those items, then that has to be achieved by identifying a specific Item & Quantity.

At this point I am noticing that I have not stored the weight anywhere. It is beginning to look like a new class
(WeighedItem) encapsulating both Item & Weight is required that should also implement hashCode and equals() to allow
the specific identification of weight based items added to the Session. These should probably be stored in a Set of
some kind.

Other intersting problems to consider would be how Voiding a unit based item like LIME might not affect the overall
price of the Session in the case where a Promotion no longer applies.


describe what REST resources and uri would be appropriate.

An Epos terminal may need to query prices across a network in order to be as up to date as posible with changes in price.
The lifecycle of an Offer may well be understood via some WebService. There may be cases where an offer is withdrawn
or extended.