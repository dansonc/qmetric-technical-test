package supermarket;

import java.util.HashMap;
import java.util.Map;


class Epos {

    private static final int SESSION_ID_BASE = -1;
    private int sessionId = SESSION_ID_BASE;

    private Session currentSession;
    private final Map<Integer, Session> sessions = new HashMap<>();
    private final Map<Item, PriceRetriever> priceRetrievers;

    Epos(Map<Item, PriceRetriever> priceRetrievers) {

        this.priceRetrievers = priceRetrievers;
    }

    void scan(Item item) {
        currentSession.add(item, priceRetrievers.get(item));
    }

    void addWeighedItem(Item item, double weightInKg) {
        currentSession.addWeighedItem(item, priceRetrievers.get(item), weightInKg);
    }

    int sessionStart() {
        currentSession = new Session();
        sessions.put(++sessionId, currentSession);

        return sessionId;
    }

    Session getSession(int sessionId) {
        return sessions.get(sessionId);
    }
}
