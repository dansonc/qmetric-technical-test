package supermarket;


import java.time.LocalDate;

class ThreeForTwo extends Offer {

    ThreeForTwo(LocalDate expiry) {
        super(expiry);
    }

    @Override
    public boolean applies(int scannedCount) {
        return scannedCount % 3 == 0 && notExpired();
    }

}
