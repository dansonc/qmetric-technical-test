package supermarket;


abstract class SkuAdaptor implements PriceRetriever{

    public double getPrice(int scannedCount){
        throw new UnsupportedOperationException();
    }

    public double getPrice(double weightInKg){
        throw new UnsupportedOperationException();
    }
}
