package supermarket;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static supermarket.Item.*;
import static supermarket.Promotion.NO_PROMOTION;

public class EposTest {

    // TODO - should not be able to sell a unit based item as a weighed item
    // TODO - consider how to Void an item
    // TODO - consider hashCode & equals and whether or not they are required
    // TODO - consider multiple Sessions and how to close a Session

    private static final double ARBITRARY_ACCEPTABLE_DELTA = 0.000000001;

    @SuppressWarnings("unchecked")
    private final Map<Item, PriceRetriever> priceRetrievers = mock(HashMap.class);

    private final Epos epos = new Epos(priceRetrievers);
    private int currentSessionId;

    private final PriceRetriever limePriceRetriever = new Sku(new ThreeForTwo(LocalDate.ofYearDay(2017, 365)), 0.15);
    private final PriceRetriever limePriceRetrieverWithExpiredOffer = new Sku(new ThreeForTwo(LocalDate.ofYearDay(2017, 1)), 0.15);
    private final PriceRetriever melonPriceRetriever = new Sku(new BuyOneGetOneFree(LocalDate.ofYearDay(2017, 365)), 0.50);
    private final PriceRetriever melonPriceRetrieverWithExpiredOffer = new Sku(new BuyOneGetOneFree(LocalDate.ofYearDay(2017, 1)), 0.50);
    private final PriceRetriever bananaPriceRetriever = new Sku(NO_PROMOTION, 0.20);
    private final PriceRetriever onionPriceRetriever = new WeighedSku(1.20); // per Kg

    @Before
    public void setup() {

        when(priceRetrievers.get(LIME)).thenReturn(limePriceRetriever);
        when(priceRetrievers.get(MELON)).thenReturn(melonPriceRetriever);
        when(priceRetrievers.get(BANANA)).thenReturn(bananaPriceRetriever);
        when(priceRetrievers.get(ONION)).thenReturn(onionPriceRetriever);
    }

    @Test
    public void scan1Lime(){

        currentSessionId = epos.sessionStart();
        scanItems(LIME);

        Assert.assertEquals(0.15, epos.getSession(currentSessionId).getTotal(), ARBITRARY_ACCEPTABLE_DELTA);
    }

    @Test
    public void scan2Limes(){

        currentSessionId = epos.sessionStart();
        scanItems(LIME, LIME);

        Assert.assertEquals(0.30, epos.getSession(currentSessionId).getTotal(), ARBITRARY_ACCEPTABLE_DELTA);
    }

    @Test
    public void scan3LimesWithinOfferPeriod(){

        currentSessionId = epos.sessionStart();
        scanItems(LIME, LIME, LIME);

        Assert.assertEquals(0.30, epos.getSession(currentSessionId).getTotal(), ARBITRARY_ACCEPTABLE_DELTA);
    }

    @Test
    public void scan3LimesWithinOfferPeriodAnd1Melon() {

        currentSessionId = epos.sessionStart();
        scanItems(LIME, LIME, LIME, MELON);

        Assert.assertEquals(0.80, epos.getSession(currentSessionId).getTotal(), ARBITRARY_ACCEPTABLE_DELTA);
    }

    @Test
    public void scan3LimesAnd2MelonsWithinOfferPeriod() {

        currentSessionId = epos.sessionStart();
        scanItems(LIME, LIME, LIME, MELON, MELON);

        Assert.assertEquals(0.80, epos.getSession(currentSessionId).getTotal(), ARBITRARY_ACCEPTABLE_DELTA);
    }

    @Test public void scan3LimesAfterOfferHasExpired(){

        when(priceRetrievers.get(LIME)).thenReturn(limePriceRetrieverWithExpiredOffer);
        currentSessionId = epos.sessionStart();

        scanItems(LIME, LIME, LIME);

        Assert.assertEquals(0.45, epos.getSession(currentSessionId).getTotal(), ARBITRARY_ACCEPTABLE_DELTA);
    }

    @Test public void scan3LimesWithinOfferPeriodAnd2MelonsAfterOfferHasExpired(){

        when(priceRetrievers.get(MELON)).thenReturn(melonPriceRetrieverWithExpiredOffer);
        currentSessionId = epos.sessionStart();

        scanItems(LIME, LIME, LIME, MELON, MELON);

        Assert.assertEquals(1.30, epos.getSession(currentSessionId).getTotal(), ARBITRARY_ACCEPTABLE_DELTA);
    }

    @Test public void scanABanana(){

        currentSessionId = epos.sessionStart();

        scanItems(BANANA);

        Assert.assertEquals(0.20, epos.getSession(currentSessionId).getTotal(), ARBITRARY_ACCEPTABLE_DELTA);
    }

    @Test public void buy500gOfOnions(){

        currentSessionId = epos.sessionStart();

        epos.addWeighedItem(ONION, 0.5);

        Assert.assertEquals(0.60, epos.getSession(currentSessionId).getTotal(), ARBITRARY_ACCEPTABLE_DELTA);
    }

    private void scanItems(Item... items) {
        for(Item item : items){
            epos.scan(item);
        }
    }
}
